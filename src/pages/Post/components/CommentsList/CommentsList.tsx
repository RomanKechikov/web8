import React from 'react';
import { Comment } from '../../../../common/types';

/**
 * Типизируем объект пропсов
 * массив comments должен быть массивом элементов Comment
 */
type Props = {
  comments?: /** */
};

export function CommentsList({ comments }: Props) {
  if (!comments || comments?.length === 0) {
    return null;
  }

  return (
    <section>
      <h3>Comments</h3>
      {comments?.map((comment) => (
        <div key={comment.id}>
          <p>
            <b>Author:</b> {comment.name}
          </p>
          <i>{comment.body}</i>
        </div>
      ))}
    </section>
  );
}
